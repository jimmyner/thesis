-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 07, 2020 at 01:50 PM
-- Server version: 5.6.43
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cp486987_thesis`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_board`
--

CREATE TABLE `tbl_board` (
  `id` int(11) NOT NULL,
  `board_name` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `create_at` date DEFAULT NULL,
  `update_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_board`
--

INSERT INTO `tbl_board` (`id`, `board_name`, `create_at`, `update_at`) VALUES
(2, 'คณะศิลปะ', '2020-02-15', NULL),
(3, 'คณะบริหาร', '2020-03-05', NULL),
(4, 'คณะอุตสาหกรรมเเละเทคโนโลยี', '2020-03-03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_branch`
--

CREATE TABLE `tbl_branch` (
  `id` int(11) NOT NULL,
  `branch_name` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `subject_id` int(11) DEFAULT NULL,
  `board_id` int(11) DEFAULT NULL,
  `create_at` date DEFAULT NULL,
  `update_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_branch`
--

INSERT INTO `tbl_branch` (`id`, `branch_name`, `subject_id`, `board_id`, `create_at`, `update_at`) VALUES
(1, 'ศิลปะอาหาร', 1, 2, '2020-03-05', NULL),
(2, 'Test', 1, 4, '2020-03-05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_refresh`
--

CREATE TABLE `tbl_refresh` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `password_new` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` int(2) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_refresh`
--

INSERT INTO `tbl_refresh` (`id`, `code`, `password_new`, `created_at`, `updated_at`, `status`) VALUES
(1, '60522110173-6', '1234', '2020-03-02 04:23:05', '2020-03-02 04:23:05', 1),
(2, '60522110173-6', '123456789', '2020-03-03 09:32:14', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_resume`
--

CREATE TABLE `tbl_resume` (
  `id` int(11) NOT NULL,
  `full_name` varchar(150) NOT NULL,
  `university` varchar(150) NOT NULL,
  `phone_number` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `resume_name` varchar(100) DEFAULT NULL,
  `path_resume` varchar(100) DEFAULT NULL,
  `florio_name` varchar(100) DEFAULT NULL,
  `path_florio` varchar(100) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `present` mediumtext,
  `create_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subject`
--

CREATE TABLE `tbl_subject` (
  `id` int(11) NOT NULL,
  `subject_name` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `board_id` int(11) DEFAULT NULL,
  `create_at` date DEFAULT NULL,
  `update_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subject`
--

INSERT INTO `tbl_subject` (`id`, `subject_name`, `board_id`, `create_at`, `update_at`) VALUES
(1, 'ศิลปะการงาน', 2, '2020-03-05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thesis`
--

CREATE TABLE `tbl_thesis` (
  `id` int(11) NOT NULL,
  `user_id` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `title` text CHARACTER SET utf8,
  `composer` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `board` varchar(100) CHARACTER SET utf8 DEFAULT '',
  `subject` varchar(100) CHARACTER SET utf8 DEFAULT '',
  `branch` varchar(100) CHARACTER SET utf8 DEFAULT '',
  `year` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `keyword` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `file_name` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `create_at` date DEFAULT NULL,
  `update_at` date DEFAULT NULL,
  `titleng` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `abstract` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coop` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `professor` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_thesis`
--

INSERT INTO `tbl_thesis` (`id`, `user_id`, `title`, `composer`, `board`, `subject`, `branch`, `year`, `keyword`, `file_name`, `create_at`, `update_at`, `titleng`, `abstract`, `coop`, `professor`) VALUES
(1, '001', ' วันหยุดยาวช่วงปีใหม่ 2020 ใครที่อยากเข้าครัวทำอาหารกินเอง ลองมาทำเมนูอาหารยอดฮิตกันเถอะ ', 'cooking.kapook.com', '2', '1', '1', '2563', 'วันหยุดยาวช่วงปีใหม่ 2020', 'thesis-1583377507.pdf', '2020-03-05', NULL, 'Long New Year Holiday 2020. Anyone who wants to go to the kitchen to cook,', '20 เมนูอาหารยอดนิยมต้อนรับปี 2020 สูตรเด็ดมาแรง', '', 'อาจารย์นัด');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_admin` varchar(255) DEFAULT '' COMMENT '1// admin 2// superadmin 3//administrator',
  `code_student` varchar(255) DEFAULT NULL,
  `board` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT '',
  `branch` varchar(255) DEFAULT NULL,
  `status` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `email`, `phone`, `username`, `password`, `created_at`, `is_admin`, `code_student`, `board`, `subject`, `branch`, `status`) VALUES
(2, 'jame0925623256@gmail.com', '0925623256', 'Nattaphon Kiattikul', 'e10adc3949ba59abbe56e057f20f883e', '2020-03-05 03:29:59', '4', '001', '1', '1', '1', 0),
(3, 'infinityp.soft@gmail.com', '0618096661', 'admin@example.com', 'e10adc3949ba59abbe56e057f20f883e', '2020-03-05 03:29:57', '2', '123456', '1', '1', '1', 0),
(4, 'test@gmail.com', '0877777887', 'เทสอย่าง มีระบบ', 'e10adc3949ba59abbe56e057f20f883e', '2020-03-05 03:29:54', '2', '654321', '1', '1', '1', 0),
(9, 'mikiboy004@gmail.com', '0618096661', 'มิกิ  อาษาวงค์', 'e10adc3949ba59abbe56e057f20f883e', '2020-03-02 04:21:12', '3', '60522110173-1', '1', '1', '1', 1),
(11, 'chinnn00500@hotmail.com', '0933368678', 'ชินวัตร พรมสุริย์', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-26 13:16:32', '2', '987654', '2', '2', '2', 0),
(12, 'test@hotmail.com', '0000000000', 'Test Test', '81dc9bdb52d04dc20036dbd8313ed055', '2020-03-02 04:37:09', '3', '60522110173-6', '3', '3', '3', 1),
(13, 'chinnn0050@hotmail.com', '0000000000', 'ชิน =bo', 'e10adc3949ba59abbe56e057f20f883e', '2020-03-02 04:50:44', '3', '595221101736', '2', '2', '2', 1),
(14, 'chin@hotmail.com', '0001110000', 'ชิน pim', 'e10adc3949ba59abbe56e057f20f883e', '2020-03-02 04:41:21', '3', '123456789', '2', '2', '2', 1),
(15, 'miki_ik@hotmail.com', '061809661', 'sadfsd sdfdf', '202cb962ac59075b964b07152d234b70', '2020-03-03 02:51:36', '3', '123456789789', '2', '2', '2', 0),
(16, 'asda@hotmail.com', '0001110000', 'ชิน narak', 'e10adc3949ba59abbe56e057f20f883e', '2020-03-03 03:27:53', '3', '111111111111', '4', '4', '4', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_board`
--
ALTER TABLE `tbl_board`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_branch`
--
ALTER TABLE `tbl_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_refresh`
--
ALTER TABLE `tbl_refresh`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_resume`
--
ALTER TABLE `tbl_resume`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_subject`
--
ALTER TABLE `tbl_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_thesis`
--
ALTER TABLE `tbl_thesis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_board`
--
ALTER TABLE `tbl_board`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_branch`
--
ALTER TABLE `tbl_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_refresh`
--
ALTER TABLE `tbl_refresh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_resume`
--
ALTER TABLE `tbl_resume`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_subject`
--
ALTER TABLE `tbl_subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_thesis`
--
ALTER TABLE `tbl_thesis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
