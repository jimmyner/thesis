<?php
defined('BASEPATH') OR exit('No direct script access allowed');



$route['default_controller'] = 'funtion/Search_ctr/search';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;



$route['Login']                 = 'funtion/Login_ctr';
$route['refresh_login']         = 'funtion/Login_ctr/refresh_login';
$route['refresh_success']       = 'funtion/Login_ctr/refresh_success';
$route['refresh_list']          = 'funtion/Refresh_ctr/refresh_list';
$route['refresh_change']        = 'funtion/Refresh_ctr/refresh_change';
$route['refresh_delete']        = 'funtion/Refresh_ctr/refresh_delete';
$route['LoginMe']               = 'funtion/Login_ctr/loginMe';
$route['LogOut']                = 'funtion/Login_ctr/logout';
// $route['profile'] = 'funtion/Login_ctr/profile'; 
$route['edit_profile']          = 'funtion/Login_ctr/edit_profile'; 
$route['edit_password']         = 'funtion/Login_ctr/edit_password'; 



$route['dashboard']             = 'funtion/Dashboard_ctr/dashboard'; 
$route['index']                 = 'funtion/Index_ctr/index'; 
$route['user']                  = 'funtion/Index_ctr/user'; 
$route['user_student']          = 'funtion/Index_ctr/user_student'; 
$route['user_teacher']          = 'funtion/Index_ctr/user_teacher'; 
$route['user_student_edit']     = 'funtion/Index_ctr/user_student_edit'; 
$route['user_student_edit_com'] = 'funtion/Index_ctr/user_student_edit_com'; 
$route['user_teacher_edit']     = 'funtion/Index_ctr/user_teacher_edit'; 
$route['user_teacher_edit_com'] = 'funtion/Index_ctr/user_teacher_edit_com'; 
$route['user_edit']             = 'funtion/Index_ctr/user_edit'; 
$route['user_edit_com']         = 'funtion/Index_ctr/user_edit_com'; 
$route['user_status']           = 'funtion/Index_ctr/status'; 
$route['status_student']        = 'funtion/Index_ctr/status_student'; 
$route['status_teacher']        = 'funtion/Index_ctr/status_teacher'; 
$route['delete_user']           = 'funtion/Index_ctr/delete_user'; 
$route['delete_user_student']   = 'funtion/Index_ctr/delete_user_student'; 
$route['delete_user_teacher']   = 'funtion/Index_ctr/delete_user_teacher'; 
$route['Register']              = 'funtion/Register_ctr'; 
$route['Register_complete']     = 'funtion/Register_ctr/regist_complete'; 
$route['fetch_state']           = 'funtion/Register_ctr/fetch_state'; 
$route['fetch_city']            = 'funtion/Register_ctr/fetch_city'; 
$route['board']                 = 'funtion/Index_ctr/board'; 
$route['board_edit']            = 'funtion/Index_ctr/board_edit'; 
$route['delete_board_edit']     = 'funtion/Index_ctr/delete_board_edit'; 
$route['subject']               = 'funtion/Index_ctr/subject'; 
$route['subject_edit']          = 'funtion/Index_ctr/subject_edit'; 
$route['delete_subject_edit']   = 'funtion/Index_ctr/delete_subject_edit'; 
$route['branch']                = 'funtion/Index_ctr/branch'; 
$route['branch_edit']           = 'funtion/Index_ctr/branch_edit'; 
$route['delete_branch_edit']    = 'funtion/Index_ctr/delete_branch_edit'; 
$route['Thesis']                = 'funtion/Thesis_ctr'; 
$route['Thesis_add_com']        = 'funtion/Thesis_ctr/thesis_add_com'; 
$route['Thesis_edit']           = 'funtion/Thesis_ctr/thesis_edit'; 
$route['Thesis_edit_com']       = 'funtion/Thesis_ctr/thesis_edit_com'; 
$route['delete_thesis']         = 'funtion/Thesis_ctr/delete_thesis'; 
$route['thesis_note']           = 'funtion/Thesis_ctr/thesis_note'; 


$route['profile']               = 'funtion/Profile_ctr/profile_all';
$route['update_status']         = 'funtion/Thesis_ctr/update_status'; 


$route['search']                = 'funtion/Search_ctr/search'; 
$route['search_edit']           = 'funtion/Search_ctr/search_edit'; 
$route['search_edit_com']       = 'funtion/Search_ctr/search_edit_com'; 
$route['search_result']         = 'funtion/Search_ctr/search_result'; 
$route['search_delete']         = 'funtion/Search_ctr/search_delete'; 
$route['search_note']           = 'funtion/Search_ctr/search_note'; 

$route['chats']                 = 'funtion/Dashboard_ctr/getdata'; 






