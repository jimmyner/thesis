<?php $id    = $this->input->get('id') ?>
<?php $user = $this->db->get_where('tbl_user', ['id' => $id])->row_array(); ?>

<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">

                    </div>
                    <h4 class="page-title">ข้อมูลผู้ใช้งานนักศึกษา</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-lg-10">
                <div class="card m-b-30">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">แก้ไขข้อมูลผู้ใช้งานนักศึกษา</h4>
                        <form class="" action="user_student_edit_com" method="POST">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="hidden" name="id" value="<?php echo $user['id'] ?>">
                                        <label>ชื่อ-สกุล</label>
                                        <input type="text" class="form-control" name="username" value="<?php echo $user['username'] ?>" required placeholder="ชื่อ-สกุล" />
                                    </div>
                                    <div class="col-sm-6">
                                        <label>อีเมล</label>
                                        <input type="text" class="form-control" name="email" value="<?php echo $user['email'] ?>" required placeholder="อีเมล" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>เบอร์โทร</label>
                                        <input type="text" class="form-control" name="phone" value="<?php echo $user['phone'] ?>" required placeholder="เบอร์โทร" />
                                    </div>
                                    <div class="col-sm-6">
                                        <label>รหัสนักศึกษา</label>
                                        <input type="text" class="form-control" name="code_student" value="<?php echo $user['code_student'] ?>" required placeholder="เบอร์โทร" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>คณะ</label>
                                        <?php $get_board = $this->db->get('tbl_board')->result_array(); ?>
                                        <select class="form-control" name="board" id="board">
                                            <option value="" selected disabled>กรุณาเลือกคณะ</option>
                                            <?php foreach ($get_board as $key => $get_board) : ?>
                                            <option  value="<?php echo $get_board['id']; ?>" <?php echo ($user['board'] == $get_board['id']) ? 'selected' : ''; ?> ><?php echo $get_board['board_name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $amp = $this->db->get('tbl_subject')->result_array(); ?>
                                        <label>สาขา</label>
                                        <select class="form-control" name="subject" id="subject">
                                            <option value="" selected disabled>กรุณาเลือกสาขา</option>
                                            <?php foreach ($amp as $key => $amp) : ?>
                                            <option  value="<?php echo $amp['id']; ?>" <?php echo ($user['subject'] == $amp['id']) ? 'selected' : ''; ?> ><?php echo $amp['subject_name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $dist = $this->db->get('tbl_branch')->result_array(); ?>
                                        <label>โปแกรมวิชา</label>
                                        <select class="form-control" name="branch" id="branch">
                                            <option value="" selected disabled>กรุณาเลือกโปแกรมวิชา</option>
                                            <?php foreach ($dist as $key => $dist) : ?>
                                            <option  value="<?php echo $dist['id']; ?>" <?php echo ($user['branch'] == $dist['id']) ? 'selected' : ''; ?> ><?php echo $dist['branch_name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        บันทึก
                                    </button>
                                    <a href="user_student"><button type="button" class="btn btn-secondary waves-effect m-l-5">
                                            ยกเลิก
                                        </button></a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div> <!-- end col -->


        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->