<?php $profile = $this->db->get_where('tbl_user', ['code_student' => $this->session->userdata('code_student')])->row_array(); ?>
<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
        
                    
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <?php if (empty($refresh)) : ?>
                    <h2 class="text-center">ไม่มีข้อมูลขอรหัสผ่านใหม่</h2>
                <?php else: ?>
                <div class="card m-b-30">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">รายการขอรหัสผ่านใหม่</h4>

                        <table id="datatable" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>ลำดับ</th>
                                    <th>รหัสนักศึกษา</th>
                                    <th>รหัสผ่านใหม่</th>
                                    <th>เครื่องมือ</th>
                                </tr>
                            </thead>
                            <?php $i = 1 ?>

                            <tbody>
                                <?php foreach ($refresh as $key => $value) : ?>
                                    <tr>
                                        <td style="text-align: center"><?php echo $i++ ?></td>
                                        <td><?php echo $value->code; ?></td>
                                        <td><?php echo $value->password_new; ?></td>
                                        <td>
                                            <?php if ($value->status == '0') : ?>
                                                <a href="refresh_change?id=<?php echo $value->id; ?>&code=<?php echo $value->code; ?>&password=<?php echo  $value->password_new; ?>" class="btn btn-info">อนุมัติ</a>
                                                <a href="refresh_delete?id=<?php echo $value->id; ?>" class="btn btn-danger" onclick="if(confirm('แน่ใจใช่ไมที่จะลบข้อมูล?')) return true; else return false;">ลบ</a>

                                            <?php else : ?>
                                                <button class="btn btn-secondary">อนุมัติ</button>
                                                <button class="btn btn-secondary">ลบ</button>
                                            <?php endif; ?>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
</div> <!-- end row -->