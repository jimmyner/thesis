<?php $id = $this->input->get('id') ?>
<?php $thesis = $this->db->get_where('tbl_thesis', ['id' => $id])->row_array(); ?>
<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">

                    </div>
                    <h4 class="page-title">ปริญญานิพนธ์นักศึกษา</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-lg-10">
                <div class="card m-b-30">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">ข้อมูลปริญญานิพนธ์นักศึกษา</h4>
                        <form class="" action="search_edit_com" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>ชื่อเรื่อง</label>
                                        <p><?php echo $thesis['title'] ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>ชื่อเรื่องภาษาอังกฤษ</label>
                                        <p><?php echo $thesis['titleng'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>ชื่อผู้แต่ง</label>
                                        <p><?php echo $thesis['composer'] ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>บทคัดย่อ</label>
                                        <p><?php echo $thesis['abstract'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>ชื่อผู้ร่วมจัดทำ ถ้าไม่มีก็ใส่เป็นค่าว่างได้</label>
                                        <p><?php echo $thesis['coop'] ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>อาจารย์ที่ปรึกษาโครงงาน</label>
                                        <p><?php echo $thesis['professor'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>ปีที่แต่ง</label>
                                        <p><?php echo $thesis['year'] ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>คำสำคัญ</label>
                                        <p><?php echo $thesis['keyword'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>คณะ</label>
                                        <?php $pro = $this->db->get_where('tbl_board', ['id' => $thesis['board']])->row_array(); ?>
                                        <p><?php echo $pro['board_name']; ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>สาขา</label>
                                        <?php $amp = $this->db->get_where('tbl_subject', ['id' => $thesis['subject']])->row_array(); ?>
                                        <p><?php echo $amp['subject_name']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>โปรแกรมวิชา</label>
                                        <?php $dist = $this->db->get_where('tbl_branch', ['id' => $thesis['branch']])->row_array(); ?>
                                        <p><?php echo $dist['branch_name']; ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>ประเภทเอกสาร</label>
                                       
                                        <p>
                                            <?php if ($thesis['category'] == '') { ?>
                                                -
                                            <?php }else{ ?>
                                                <?php echo $thesis['category']; ?>
                                            <?php } ?>
                                            
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div>
                                    <a href="search">
                                        <button type="button" class="btn btn-secondary waves-effect m-l-5"> ยกเลิก </button>
                                    </a>
                                    <?php if ($this->session->userdata('code_student') != '') : ?>
                                    <a href="./uploads/thesis/<?php echo $thesis['file_name']; ?>" class="btn btn-info" target="_blank" data-toggle="tooltip" data-placement="bottom" title="ดาวโหลด"><i class="fa fa-arrow-circle-down"></i> ดาวโหลด</a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div> <!-- end col -->


        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->