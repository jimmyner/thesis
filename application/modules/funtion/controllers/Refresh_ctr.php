<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Refresh_ctr extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function refresh_list()
    {
        if ($this->session->userdata('code_student') != '') {
            
            $data['refresh'] = $this->db->get('tbl_refresh')->result();
            
            $this->load->view('option/header');
            $this->load->view('refresh_list',$data);
            $this->load->view('option/footer');
        } else {
            $this->load->view('login');
        }
    }

    public function refresh_change()
    {
        if ($this->session->userdata('code_student') != '') {
            $id         = $this->input->get('id');
            $code       = $this->input->get('code');
            $password   = $this->input->get('password');
            $check      = $this->db->get_where('tbl_user',['code_student' => $code])->row();
            if ($check == true) 
            {
                $update = array(
                    'status' => 1
                );
                $this->db->where('id', $id);
                if ($this->db->update('tbl_refresh', $update)) 
                {
                    $data = array(
                        'password' => md5($password)
                    );
                    $this->db->where('code_student', $code);
                    $success = $this->db->update('tbl_user', $data);
                } 

                //////////////// $success ////////////////

                if ($success > 0) {
                    $this->session->set_flashdata('save_ss2', 'เปลี่ยนรหัวผ่านใหม่เรียบร้อยแล้ว.');
                    redirect('refresh_list', 'refresh');
                }
                else 
                {
                    $this->session->set_flashdata('del_ss2', 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง!');
                    redirect('refresh_list', 'refresh');
                }
                
            }
            else
            {
                $this->session->set_flashdata('del_ss2', 'ไม่มีรหัสนักศึกษานี้อยู่ในระบบ!');
                redirect('refresh_list', 'refresh');
            }

            
           
        } else {
            $this->load->view('login');
        }
    }

    public function  refresh_delete()
    {
        $id = $this->input->get('id');


        $this->db->where('id', $id);
        $resultsedit = $this->db->delete('tbl_refresh', ['id' => $id]);

        if ($resultsedit > 0) {
            $this->session->set_flashdata('save_ss2', ' Successfully  ลบข้อมูลเรียบร้อยแล้ว  !!.');
        } else {
            $this->session->set_flashdata('del_ss2', 'Not Successfully  ไม่สามารถลบข้อมูลได้ !!.');
        }
        return redirect('refresh_list');
    }

  
}
