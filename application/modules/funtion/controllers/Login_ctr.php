<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_ctr extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->session->userdata('email') != '') {
            redirect('index');
        } else {
            $this->load->view('login');
        }
    }

    public function refresh_login()
    {
        $this->load->view('refresh_login');
    }

    public function refresh_success()
    {
        $code       = $this->input->post('code');
        $password_new   = $this->input->post('password_new');
        $check      = $this->db->get_where('tbl_user', ['code_student' => $code, 'status' => '0' ])->row();
        $check2      = $this->db->get_where('tbl_user', ['code_student' => $code, 'is_admin !=' => '3' ])->row();

        if ($check2 == true) {
            $this->session->set_flashdata('del_ss2', 'ท่านไม่ใช่นักศึกษาในการขอรหัสใหม่!');
            redirect('refresh_login', 'refresh');
        }
        if ($check == true) {
            $this->session->set_flashdata('del_ss2', 'Admin ยังไม่ได้ตรวจสอบรหัสของท่าน !');
            redirect('refresh_login', 'refresh');
        } else {
            $data = array(
                'code'          => $code,
                'password_new'  => $password_new,
                'created_at'    => date('Y-m-d H:i:s'),
            );
            $success = $this->db->insert('tbl_refresh', $data);

            if ($success > 0) {
                $this->session->set_flashdata('save_ss2', 'ขอรหัสผ่านใหม่เรียบร้อยแล้ว กรุณารอการตรวจสอบจาก Admin.');
            } else {
                $this->session->set_flashdata('del_ss2', 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง!');
            }
            redirect('Login', 'refresh');
        }
    }

    public function edit_profile()
    {
        $id = $this->input->post('id');

        $data = array(
            'code_student'  => $this->input->post('code'),
            'username'      => $this->input->post('username'),
            'email'         => $this->input->post('email'),
            'phone'         => $this->input->post('tel'),
        );

        $this->db->where('id', $id);
        $success = $this->db->update('tbl_user', $data);

        if ($success > 0) {
            $this->session->set_flashdata('save_ss2', 'บันมึกข้อมูลเรัยบร้อยแล้ว.');
        } else {
            $this->session->set_flashdata('del_ss2', 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง!');
        }
        redirect('profile', 'refresh');
    }


    public function loginMe()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('code_student', 'code_student', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run()) {
            $code_student = $this->input->post('code_student');
            $password = md5($this->input->post('password'));
            $this->load->model('Login_model');

            if ($this->Login_model->login($code_student, $password)) {
                $user_data = array(
                    'code_student' => $code_student
                );

                $this->session->set_userdata($user_data);
                $this->session->set_flashdata('save_ss2', 'ล็อกอินเรียบร้อยแล้ว');
                redirect('dashboard');
            } elseif ($this->Login_model->login_student($code_student, $password)) {
                $user_data = array(
                    'code_student' => $code_student
                );

                $this->session->set_userdata($user_data);
                $this->session->set_flashdata('save_ss2', 'ล็อกอินเรียบร้อยแล้ว');
                redirect('index');
            } elseif ($this->Login_model->login_teacher($code_student, $password)) {
                $user_data = array(
                    'code_student' => $code_student
                );

                $this->session->set_userdata($user_data);
                $this->session->set_flashdata('save_ss2', 'ล็อกอินเรียบร้อยแล้ว');
                redirect('index');
            } else {
                $this->session->set_flashdata('del_ss2', 'รหัสผ่านหรือชื่อผู้ใช้งานผิด หรือ กรุณารอการตรวจสอบจาก Admin ภายใน 24 ชั่วโมง !!');
                redirect('Login', 'refresh');
            }
        }
    }

    public function edit_password()
    {


        $id                    = $this->input->post('id');
        $password            = md5($this->input->post('password'));
        $newpassword        = $this->input->post('newpassword');
        $confirmpassword    = $this->input->post('confirmpassword');
        $check = $this->db->get_where('tbl_user', ['password' => $password])->row_array();
        if ($check == false) {
            echo "<script>";
            echo "alert('รหัสของท่านไม่ตรงกับอันเก่า โปรดกรอกอีกครั้ง');";
            echo "window.location='profile';";
            echo "</script>";
        }

        if ($newpassword != $confirmpassword) {
            echo "<script>";
            echo "alert('รหัสผ่านของท่านไม่ตรงกัน !!');";
            echo "window.location='profile';";
            echo "</script>";
        } elseif ($newpassword == $confirmpassword) {
            $data = array(
                'password'            => md5($newpassword)
            );
        } else {
            echo "<script>";
            echo "alert('Password incorrect.Try again!!');";
            echo "window.location='profile';";
            echo "</script>";
        }
        $this->db->where('id', $id);
        if ($this->db->update('tbl_user', $data)) {
            echo "<script>";
            echo "alert('บันทึกรหัสผ่านใหม่เรียบร้อยแล้ว.');";
            echo "window.location='profile';";
            echo "</script>";
        } else {
            echo "<script>";
            echo "alert('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง.');";
            echo "window.location='profile';";
            echo "</script>";
        }
    }

    public function logout()
    {
        $this->session->sess_destroy(); //ล้างsession

        redirect('Login'); //กลับไปหน้า Login
    }
}
