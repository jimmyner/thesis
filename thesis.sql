/*
Navicat MySQL Data Transfer

Source Server         : db
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : thesis

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-05-23 16:16:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_board`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_board`;
CREATE TABLE `tbl_board` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `board_name` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `create_at` date DEFAULT NULL,
  `update_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_board
-- ----------------------------
INSERT INTO `tbl_board` VALUES ('2', 'คณะศิลปะ', '2020-04-30', null);
INSERT INTO `tbl_board` VALUES ('3', 'คณะบริหาร5', '2020-04-30', null);
INSERT INTO `tbl_board` VALUES ('4', 'คณะอุตสาหกรรมเเละเทคโนโลยี', '2020-03-03', null);
INSERT INTO `tbl_board` VALUES ('5', 'การตลาดTH', '2020-04-30', null);

-- ----------------------------
-- Table structure for `tbl_branch`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_branch`;
CREATE TABLE `tbl_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `subject_id` int(11) DEFAULT NULL,
  `board_id` int(11) DEFAULT NULL,
  `create_at` date DEFAULT NULL,
  `update_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_branch
-- ----------------------------
INSERT INTO `tbl_branch` VALUES ('1', 'ศิลปะอาหาร', '1', '2', '2020-04-30', null);
INSERT INTO `tbl_branch` VALUES ('2', 'TestTS', '1', '4', '2020-04-30', null);

-- ----------------------------
-- Table structure for `tbl_refresh`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_refresh`;
CREATE TABLE `tbl_refresh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `password_new` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `status` int(2) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_refresh
-- ----------------------------
INSERT INTO `tbl_refresh` VALUES ('1', '60522110173-6', '1234', '2020-03-02 11:23:05', '2020-03-02 11:23:05', '1');
INSERT INTO `tbl_refresh` VALUES ('4', '111111111111', '8547684', '2020-04-29 22:42:05', null, '0');
INSERT INTO `tbl_refresh` VALUES ('5', '123456789', '123', '2020-04-29 22:43:32', null, '0');

-- ----------------------------
-- Table structure for `tbl_resume`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_resume`;
CREATE TABLE `tbl_resume` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(150) NOT NULL,
  `university` varchar(150) NOT NULL,
  `phone_number` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `resume_name` varchar(100) DEFAULT NULL,
  `path_resume` varchar(100) DEFAULT NULL,
  `florio_name` varchar(100) DEFAULT NULL,
  `path_florio` varchar(100) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `present` mediumtext DEFAULT NULL,
  `create_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_resume
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_subject`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_subject`;
CREATE TABLE `tbl_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `board_id` int(11) DEFAULT NULL,
  `create_at` date DEFAULT NULL,
  `update_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_subject
-- ----------------------------
INSERT INTO `tbl_subject` VALUES ('1', 'ศิลปะการงาน', '2', '2020-04-30', null);
INSERT INTO `tbl_subject` VALUES ('2', 'BIS1', '3', '2020-04-30', null);
INSERT INTO `tbl_subject` VALUES ('3', 'BIT44', '4', '2020-04-30', null);
INSERT INTO `tbl_subject` VALUES ('4', 'MRT EN', '5', '2020-04-30', null);
INSERT INTO `tbl_subject` VALUES ('5', 'SMG01', '5', '2020-04-30', null);

-- ----------------------------
-- Table structure for `tbl_thesis`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_thesis`;
CREATE TABLE `tbl_thesis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `title` text CHARACTER SET utf8 DEFAULT NULL,
  `composer` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `board` varchar(100) CHARACTER SET utf8 DEFAULT '',
  `subject` varchar(100) CHARACTER SET utf8 DEFAULT '',
  `branch` varchar(100) CHARACTER SET utf8 DEFAULT '',
  `year` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `keyword` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `file_name` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `create_at` date DEFAULT NULL,
  `update_at` date DEFAULT NULL,
  `titleng` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `abstract` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coop` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `professor` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category` varchar(255) CHARACTER SET utf8 DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_thesis
-- ----------------------------
INSERT INTO `tbl_thesis` VALUES ('1', '60522110173-6', ' วันหยุดยาวช่วงปีใหม่ 2020 ใครที่อยากเข้าครัวทำอาหารกินเอง ลองมาทำเมนูอาหารยอดฮิตกันเถอะ ', 'cooking.kapook.com', '2', '1', '1', '2563', 'วันหยุดยาวช่วงปีใหม่ 2020', 'thesis-1583377507.pdf', '2020-03-05', '2020-04-30', 'Long New Year Holiday 2020. Anyone who wants to go to the kitchen to cook,', '20 เมนูอาหารยอดนิยมต้อนรับปี 2020 สูตรเด็ดมาแรง', '', 'อาจารย์นัด1', null);
INSERT INTO `tbl_thesis` VALUES ('2', '60522110173-6', 'ลองกองT', 'nlovecooking.com', '2', '1', '1', '2563', 'EMT123456789', 'thesis-1587809093.png', '2020-04-25', '2020-04-30', 'asdasd', '20 เมนูอาหารยอดนิยมต้อนรับปี 2020 สูตรเด็ดมาแรง', 'หกฟหกฟหก', 'อาจารย์นัด', null);
INSERT INTO `tbl_thesis` VALUES ('3', '595221101736', 'asdasd', 'nlovecooking.com', '2', '1', '1', '2563', 'EMT123456789', 'thesis-1587812041.pdf', '2020-04-25', null, 'ฟกหฟหกฟหก', 'หกฟหกฟหก', 'หกฟหกฟหก', 'หกฟหกฟหก', null);
INSERT INTO `tbl_thesis` VALUES ('4', '60522110173-1', 'สูตรอาหารไทย', 'www.thairath.co.th', '2', '1', '1', '2563', 'Key', 'thesis-1590219849.png', '2020-05-23', null, 'Work From Home', '30 เมนูอาหารง่ายๆ ทำเองได้ ', '', 'อาจารย์นัด', 'โครงการ');

-- ----------------------------
-- Table structure for `tbl_user`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `is_admin` varchar(255) DEFAULT '' COMMENT '1// admin 2// superadmin 3//administrator',
  `code_student` varchar(255) DEFAULT NULL,
  `board` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT '',
  `branch` varchar(255) DEFAULT NULL,
  `status` int(2) DEFAULT 0 COMMENT '1 admin / 2 อาจารย์ / 3 นักเรียน',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO `tbl_user` VALUES ('2', 'jame0925623256@gmail.com', '0925623251', 'Nattaphon Kiattikul', 'e10adc3949ba59abbe56e057f20f883e', '2020-04-25 17:32:36', '4', '001', '1', '1', '1', '0');
INSERT INTO `tbl_user` VALUES ('3', 'infinityp.soft@gmail.com', '0618096662', 'admin@example.com', 'e10adc3949ba59abbe56e057f20f883e', '2020-04-30 13:24:55', '3', '123456', null, '1', '1', '0');
INSERT INTO `tbl_user` VALUES ('4', 'test@gmail.com', '0877777887', 'เทสอย่าง มีระบบ', 'e10adc3949ba59abbe56e057f20f883e', '2020-04-30 12:41:23', '2', '654321', '2', '1', '2', '0');
INSERT INTO `tbl_user` VALUES ('9', 'mikiboy004@gmail.com', '0618096660', 'มิกิ  อาษาวงค์', 'e10adc3949ba59abbe56e057f20f883e', '2020-04-18 18:23:35', '3', '60522110173-1', '2', '1', '1', '1');
INSERT INTO `tbl_user` VALUES ('11', 'chinnn00500@hotmail.com', '0933368675', 'ชินวัตร พรมสุริย์', 'e10adc3949ba59abbe56e057f20f883e', '2020-04-18 18:23:55', '2', '987654', '2', '2', '2', '0');
INSERT INTO `tbl_user` VALUES ('12', 'test@hotmail.com', '0000000000', 'Test Test', 'e10adc3949ba59abbe56e057f20f883e', '2020-04-25 17:29:16', '2', '60522110173-6', '3', '3', '3', '1');
INSERT INTO `tbl_user` VALUES ('13', 'chinnn0050@hotmail.com', '0000000000', 'ชิน =bo', 'e10adc3949ba59abbe56e057f20f883e', '2020-03-02 11:50:44', '3', '595221101736', '2', '2', '2', '1');
INSERT INTO `tbl_user` VALUES ('14', 'chin@hotmail.com', '0001110000', 'ชิน pim', 'e10adc3949ba59abbe56e057f20f883e', '2020-03-02 11:41:21', '3', '123456789', '2', '2', '2', '1');
INSERT INTO `tbl_user` VALUES ('15', 'miki_ik@hotmail.com', '061809661', 'sadfsd sdfdf', '202cb962ac59075b964b07152d234b70', '2020-03-03 09:51:36', '3', '123456789789', '2', '2', '2', '0');
INSERT INTO `tbl_user` VALUES ('16', 'asda@hotmail.com', '0001110000', 'ชิน narak', 'e10adc3949ba59abbe56e057f20f883e', '2020-03-03 10:27:53', '3', '111111111111', '4', '4', '4', '0');
